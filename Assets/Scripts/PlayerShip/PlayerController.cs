﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceShip;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;

        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            if (GameManager.Instance.GetGameLevel() == 1)
            {
                playerSpaceShip.Fire();
            }
            else if (GameManager.Instance.GetGameLevel() == 2)
            {
                playerSpaceShip.ThreeShotFire();
            }
            else
            {
                playerSpaceShip.Fire();
            }
            
        }

        public void OnMove (InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceShip.Speed;

            var garvityForce = new Vector2(0, -1);
            
            var finalVelocity = inputVelocity + garvityForce;

            var newPosition = transform.position;
            newPosition.x = transform.position.x + finalVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + finalVelocity.y * Time.smoothDeltaTime;
        
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);
                    
            Debug.DrawRay(transform.position, inputVelocity, Color.green);
            Debug.DrawRay(transform.position, finalVelocity, Color.white);
            Debug.DrawRay(transform.position, garvityForce, Color.yellow);

            transform.position = newPosition;
        }
        
        private void CreateMovementBoundary()
        {
            Camera mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main camera cannot be null");

            var spriteRenderer = playerSpaceShip.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
