using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;

        public static EnemySpaceShip Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            if (Hp <= 0)
            {
                return;
            }
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
            Debug.Assert(Hp <= 0,"HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
        
        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.EnemyBullet);
                if (bullet)
                {
                    bullet.transform.position = gunPosition.position;
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bullet.GetComponent<Bullet>().Init(Vector2.down);    
                }
                fireCounter = 0;
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
            }
            
        }
    }
}

