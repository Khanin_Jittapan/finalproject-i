using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private Transform[] threeShotGun;
        
        public  static PlayerSpaceShip Instance { get; private set; }

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public override void Fire()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet)
            {
                bullet.transform.position = gunPosition.position;
                bullet.transform.rotation = Quaternion.identity;
                bullet.SetActive(true);
                bullet.GetComponent<Bullet>().Init(Vector2.up);
            }
        }

        public void ThreeShotFire()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            foreach (var gun in threeShotGun)
            {
                var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
                if (bullet)
                {
                    bullet.transform.position = gun.position;
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bullet.GetComponent<Bullet>().Init(Vector2.up);
                }
            }
        }
        
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }
        
        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExplode);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}

