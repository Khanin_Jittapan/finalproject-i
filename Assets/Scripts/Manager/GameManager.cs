using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceShip playerSpaceship;
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private int playerSpaceShipHp;
        [SerializeField] private int playerSpaceShipMoveSpeed;
        [SerializeField] private int enemySpaceShipHp;
        [SerializeField] private int enemySpaceShipMoveSpeed;
        
        private int gameLevels = 0;
        
        public event Action OnRestarted;
        
        public  static GameManager Instance { get; private set; }

        [HideInInspector]
        public PlayerSpaceShip SpawnedPlayerShip;

        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceShip != null, "enemySpaceShip cannot be null");
            Debug.Assert(playerSpaceShipHp > 0, "playerSpaceShip Hp has to be more than zero");
            Debug.Assert(playerSpaceShipMoveSpeed > 0, "playerSpaceShipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceShipHp > 0, "enemySpaceShip Hp has to be more than zero");
            Debug.Assert(enemySpaceShipMoveSpeed > 0, "enemySpaceShipMoveSpeed has to be more than zero");
            DontDestroyOnLoad(GameObject.FindWithTag("Manager"));
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void StartGame()
        {
            DestroyRemainingShips();
            gameLevels = 0;
            SpawnPlayerSpaceShip();
            SpawnEnemySpaceShip();
            gameLevels++;
        }

        private void CheckLevel()
        {
            if (ScoreManager.Instance.GetScore() == 1)
            {
                gameLevels++;
                SceneManager.LoadScene("LevelTwo");
                SpawnEnemySpaceShip();
                
            }
            ScoreManager.Instance.UpdateScore(0);
        }

        private void SpawnPlayerSpaceShip()
        {
            SpawnedPlayerShip = Instantiate(playerSpaceship);
            SpawnedPlayerShip.Init(playerSpaceShipHp, playerSpaceShipMoveSpeed);
            SpawnedPlayerShip.OnExploded += OnPlayerSpaceShipExploded;
        }

        private void OnPlayerSpaceShipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceShip()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceShip);
            spawnedEnemyShip.Init(enemySpaceShipHp, enemySpaceShipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceShipExploded;
            
            if (gameLevels == 2)
            {
                var spawnedEnemyShip1 = Instantiate(enemySpaceShip);
                spawnedEnemyShip1.transform.position += new Vector3(3, 1);
                spawnedEnemyShip1.Init(enemySpaceShipHp, enemySpaceShipMoveSpeed);
                spawnedEnemyShip1.OnExploded += OnEnemySpaceShipExploded;
                
                var spawnedEnemyShip2 = Instantiate(enemySpaceShip);
                spawnedEnemyShip2.transform.position += new Vector3(-3, 1);
                spawnedEnemyShip2.Init(enemySpaceShipHp, enemySpaceShipMoveSpeed);
                spawnedEnemyShip2.OnExploded += OnEnemySpaceShipExploded;
            }
        }

        private void OnEnemySpaceShipExploded()
        {
            ScoreManager.Instance.UpdateScore(1);
            CheckLevel();
            if (ScoreManager.Instance.GetScore() >= 4)
            {
                Restart();
            }
        }

        public void Restart()
        {
            OnRestarted?.Invoke();
            DestroyRemainingShips();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

        public int GetGameLevel()
        {
            return gameLevels;
        }
    }
}

