using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private RectTransform startDialog;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private void Awake()
        {
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            quitButton.onClick.AddListener(OnQuitButtonClicked);
        }

        private void Start()
        {
            GameManager.Instance.OnRestarted += RestartUI;
            ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
            if (GameManager.Instance.GetGameLevel() != 2)
            {
                ShowStartDialog(true);
                ShowEndDialog(false);
                ShowScore(false);
            }
            UpdateScoreUI();
        }

        private void OnStartButtonClicked()
        {
            ShowStartDialog(false);
            ShowScore(true);
            GameManager.Instance.StartGame();
        }
        
        private void OnRestartButtonClicked()
        {
            ShowEndDialog(false);
            UpdateScoreUI();
            ShowScore(true);
            if (GameManager.Instance.GetGameLevel() >= 2)
            {
                SceneManager.LoadScene(0);
                return;
            }
            GameManager.Instance.StartGame();
        }

        private void OnQuitButtonClicked()
        {
            Application.Quit();
            Debug.Log("Game is exiting");
        }

        private void UpdateScoreUI()
        {
            scoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
            finalScoreText.text = $"Your Score : {ScoreManager.Instance.GetScore()}";
        }
        
        private void RestartUI()
        {
            ShowScore(false);
            ShowStartDialog(false);
            ShowEndDialog(true);
        }
        
        public void ShowScore(bool show)
        {
            scoreText.gameObject.SetActive(show);
        }

        public void ShowStartDialog(bool show)
        {
            startDialog.gameObject.SetActive(show);
        }

        public void ShowEndDialog(bool show)
        {
            endDialog.gameObject.SetActive(show);
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnRestarted -= RestartUI;
            ScoreManager.Instance.OnScoreUpdated -= UpdateScoreUI;
        }
    }
}

