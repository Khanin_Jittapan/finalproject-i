﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;

        public static SoundManager Instance { get; private set; }
        public enum Sound
        {
           BackgroundMusic,
           PlayerFire,
           PlayerExplode,
           EnemyFire,
           EnemyExplode
        }
        
        [Serializable]
        public class SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0,1)] public float soundVolume;
            public bool loop = false;
            public AudioSource AudioSource;
        }


        private void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0 ,"sound clips need to morn");
            //Debug.Assert(audioSources != null,"audioSource cannot be null");

            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
            
        }

        private void Start()
        {
            Play(Sound.BackgroundMusic);
        }

        public void Play(Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            if (soundClip.AudioSource == null)
            {   
                soundClip.AudioSource = gameObject.AddComponent<AudioSource>();
            }
            soundClip.AudioSource.clip = soundClip.audioClip;
            soundClip.AudioSource.volume = soundClip.soundVolume;
            soundClip.AudioSource.loop = soundClip.loop;
            soundClip.AudioSource.Play();
        }

        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }
            return null;
        }
    }
}
