using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        public event Action OnScoreUpdated;
        
        private int score = 0;

        private GameManager gameManager;
        
        public static ScoreManager Instance { get; private set; }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
        }

        public void UpdateScore(int score)
        {
            this.score += score;
            OnScoreUpdated?.Invoke();
        }

        public int GetScore()
        {
            return score;
        }

        public void ResetScore()
        {
            score = 0;
        }

        internal void OnRestarted()
        {
            ResetScore();
        }
    }
}

