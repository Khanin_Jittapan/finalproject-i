﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using Manager;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private float chasingThresholdDistance;

        private PlayerSpaceShip spawnedPlayerShip;

        private void Start()
        {
            spawnedPlayerShip = GameManager.Instance.SpawnedPlayerShip;
        }

        private void Update()
        {
            MoveToPlayer();
            enemySpaceShip.Fire();
        }

        private void MoveToPlayer()
        {
            if (spawnedPlayerShip == null)
                return;
            var position = transform.position;
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position,
                position);
            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2) (spawnedPlayerShip.transform.position - position);
                direction.Normalize();
                var distance = direction * enemySpaceShip.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);

            }
        }
    }    
}

